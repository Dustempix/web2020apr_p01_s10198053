
/* NP Book - WEB Module        */
/* DatabaseSetup.sql           */
/* Creation Date 22/2/2018     */

create database NPBookWEB
go

use NPBookWEB

/*** Delete tables before creating ***/

/* Table: dbo.Loan */
if exists (select * from sysobjects 
  where id = object_id('dbo.Loan') and sysstat & 0xf = 3)
  drop table dbo.Loan
GO

/* Table: dbo.BookCopy */
if exists (select * from sysobjects 
  where id = object_id('dbo.BookCopy') and sysstat & 0xf = 3)
  drop table dbo.BookCopy
GO

/* Table: dbo.Student */
if exists (select * from sysobjects 
  where id = object_id('dbo.Student') and sysstat & 0xf = 3)
  drop table dbo.Student
GO

/* Table: dbo.Member */
if exists (select * from sysobjects 
  where id = object_id('dbo.Member') and sysstat & 0xf = 3)
  drop table dbo.Member
GO


/* Table: dbo.StaffContact */
if exists (select * from sysobjects 
  where id = object_id('dbo.StaffContact') and sysstat & 0xf = 3)
  drop table dbo.StaffContact
GO

/* Drop foreign key constraint in dbo.Branch to dbo.Staff */
if exists (select * from sysobjects 
  where id = object_id('dbo.Branch') and sysstat & 0xf = 3)
  ALTER TABLE dbo.Branch
  DROP CONSTRAINT FK_Branch_MgrID 
GO

/* Table: dbo.Staff */
if exists (select * from sysobjects 
  where id = object_id('dbo.Staff') and sysstat & 0xf = 3)
  drop table dbo.Staff
GO

/* Table: dbo.Branch */
if exists (select * from sysobjects 
  where id = object_id('dbo.Branch') and sysstat & 0xf = 3)
  drop table dbo.Branch
GO

/* Table: dbo.BookAuthor */
if exists (select * from sysobjects 
  where id = object_id('dbo.BookAuthor') and sysstat & 0xf = 3)
  drop table dbo.BookAuthor
GO

/* Table: dbo.Book */
if exists (select * from sysobjects 
  where id = object_id('dbo.Book') and sysstat & 0xf = 3)
  drop table dbo.Book
GO

/* Table: dbo.Author */
if exists (select * from sysobjects 
  where id = object_id('dbo.Author') and sysstat & 0xf = 3)
  drop table dbo.Author
GO

/* Table: dbo.BookCategory */
if exists (select * from sysobjects 
  where id = object_id('dbo.BookCategory') and sysstat & 0xf = 3)
  drop table dbo.BookCategory
GO

/* Table: dbo.Publisher */
if exists (select * from sysobjects 
  where id = object_id('dbo.Publisher') and sysstat & 0xf = 3)
  drop table dbo.Publisher
GO


/*** Create tables ***/
/* Table: dbo.Branch  */
CREATE TABLE dbo.Branch 
(
  BranchNo   int   ,
  Address   varchar (255)  NULL,
  TelNo   varchar (15)  NULL,
  DateStart  datetime NULL,
  MgrID   int  NULL,
  CONSTRAINT PK_Branch PRIMARY KEY (BranchNo)
)
GO

/* Table: dbo.Staff  */
CREATE TABLE dbo.Staff 
(
  StaffID   int   IDENTITY NOT NULL ,
  Name    varchar (50)  NOT NULL,
  Gender   char (1)  NULL,
  DOB   datetime  NULL,
  DateJoin  datetime  NULL,
  Salary  smallmoney NULL,
  Nationality  varchar(50) NULL,
  BranchNo  int  NULL,
  SupervisorID  int  NULL,
  EmailAddr  varchar (255) NULL,
  Pwd  varchar (255) NULL,
  Status  bit  NULL,
  CONSTRAINT PK_Staff PRIMARY KEY (StaffID),
  CONSTRAINT FK_Staff_BranchNo FOREIGN KEY (BranchNo) REFERENCES
  dbo.Branch(BranchNo),
  CONSTRAINT FK_Staff_SupervisorID FOREIGN KEY (SupervisorId) REFERENCES
  dbo.Staff(StaffID)
)
GO

/* Add foreign key constraint to dbo.Branch */
ALTER TABLE dbo.Branch
  ADD CONSTRAINT FK_Branch_MgrID FOREIGN KEY (MgrID) REFERENCES      
  dbo.Staff(StaffID)
GO

/* Table: dbo.StaffContact  */
CREATE TABLE dbo.StaffContact 
(
  StaffID   int   ,
  ContactNo   char (10)  ,
  CONSTRAINT PK_StaffContact PRIMARY KEY (StaffID,ContactNo),
  CONSTRAINT FK_StaffContact_StaffID FOREIGN KEY (StaffID) REFERENCES
  dbo.Staff(StaffID)
)
GO


/* Insert rows */
insert into Staff (name, gender, dob, datejoin, salary, nationality, supervisorid, emailaddr, status) values ('Dominic','M','1981-01-01','2002-09-09',1500,'Malaysia',null,'richard@np40book.com.sg', 1 )

insert into Staff (name, gender, dob, datejoin, salary, nationality, supervisorid, emailaddr, status) values ('Brad','M','1982-02-02','2002-09-09',1500,'Malaysia',null,'john@np40book.com.sg', 1)

insert into Staff (name, gender, dob, datejoin, salary, nationality, supervisorid, emailaddr, status) values ('Elizabeth','F','1983-03-03','2001-09-03',1970,'Malaysia',null,'mary@np40book.com.sg', 0)

insert into Staff (name, gender, dob, datejoin, salary, nationality, supervisorid, emailaddr, status) values ('Tom','F','1974-04-14','2002-10-22',1300,'Singapore',null,'sunsun@np40book.com.sg', 1)

insert into Staff (name, gender, dob, datejoin, salary, nationality, supervisorid, emailaddr, status) values ('Audrey','F','1981-02-15','2002-10-22',1390,'Singapore',null,'jane@np40book.com.sg', 0)

insert into Staff (name, gender, dob, datejoin, salary, nationality, supervisorid, emailaddr, status) values ('Sanjay','M','1981-01-01','2001-09-03',2100,'Singapore',null,'nana@np40book.com.sg', 1)

insert into Staff (name, gender, dob, datejoin, salary, nationality, supervisorid, emailaddr, status) values ('Marilyn','F','1980-07-27','2001-09-03',1990,'China',null,'maymay@np40book.com.sg', 1)

insert into Staff (name, gender, dob, datejoin, salary, nationality, supervisorid, emailaddr, status) values ('Fatimah','F','1981-01-01','2001-09-03',1450,'China',7,'sadiah@np40book.com.sg', 1)

insert into Staff (name, gender, dob, datejoin, salary, nationality, supervisorid, emailaddr, status) values ('Chin Chong','M','1981-01-01','2001-09-03',1350,'China',7,'samuel@np40book.com.sg', 1)

update Staff 
   set SupervisorID = 3
   where StaffId = 1

update Staff 
   set SupervisorID = 5
   where StaffId = 4

update Staff 
   set SupervisorID = 6
   where StaffId = 5


insert into StaffContact values (1,'67654321')
insert into StaffContact values (1,'97654322')
insert into StaffContact values (3,'67654323')
insert into StaffContact values (4,'67654324')
insert into StaffContact values (7,'67654327')

insert into Branch values (1,'Ngee Ann Polytechnic','61111111','03-Sep-2001',3)
insert into Branch values (2,'Plaza Singapura','62222222','01-Oct-2001',6)
insert into Branch values (3,'Tampines Mall','63333333','15-Sep-2001',7)

update Staff 
  set BranchNo = 1 
  where StaffID between 1 and 3

update Staff 
  set BranchNo = 2 
  where StaffID between 4 and 6

update Staff 
  set BranchNo = 3 
  where StaffID between 7 and 9