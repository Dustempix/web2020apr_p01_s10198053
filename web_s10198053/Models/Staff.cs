﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace web_s10198053.Models
{
    public class Staff
    {
        [Display (Name = "ID")]
        public int StaffId { get; set; }

        [Required]
        [StringLength(50)]
        [Display (Name = "Name")]
        public string Name { get; set; }

        [Display (Name = "Gender")]
        public char Gender { get; set; }

        [Display (Name = "Date of Birth")]
        [DataType(DataType.Date)]
        public DateTime? DOB { get; set; }

        [Display (Name = "Nationality")]
        public string Nationality { get; set; }

        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}")]
        [Display(Name = "Email Address")]
        [EmailAddress] // Validation Annotation for email address format
        // Custom Validation Attribute for checking email address exists
        [ValidateEmailExists]
        public string Email { get; set; }

        [Range(1.00, 10000.00)]
        [Display(Name = "Monthly Salary (SGD)")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:#,##0.00}")]
        public decimal Salary { get; set; }

        [Display(Name = "Full-Time Staff")]
        public bool IsFullTime { get; set; }

        [Display(Name = "Branch")]
        public int? BranchNo { get; set; }
    }
}
