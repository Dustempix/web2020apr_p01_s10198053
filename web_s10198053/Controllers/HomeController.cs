﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using web_s10198053.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using System.Net.Http.Headers;
using System.Net.Http;
using Newtonsoft.Json;

namespace web_s10198053.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]

        public ActionResult StaffLogin(IFormCollection formData)
        {
            // Read inputs from textboxes
            // Email address converted to lowercase
            string loginID = formData["txtLoginID"].ToString().ToLower();
            string password = formData["txtPassword"].ToString();

            if (loginID == "abc@npbook.com" && password == "pass1234")
            {
                // Store Login ID in session with the Key "LoginID"
                HttpContext.Session.SetString("LoginID", loginID);
                // Store user role "Staff" as a string in session with the key "Role" 
                HttpContext.Session.SetString("Role", "Staff");
                // Store login datetime in session as a string
                HttpContext.Session.SetString("Datetime",@DateTime.Now.ToString());
                // Redirect user to the "staffMain" view through an action
                return RedirectToAction("StaffMain");
            }
            else
            {
                // Store an error message in TempData for display at the Index view
                TempData["Message"] = "Invalid Login Credentials!";
                // Redirect user back to the index view through an action
                return RedirectToAction("Index");
            }
        }

        [Authorize]
        public async Task<ActionResult> StudentLogin()
        {
            // Retrieve the access token of the user
            string accessToken = await HttpContext.GetTokenAsync("access_token");

            // Call API to obtain user information
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://ictonejourney.com");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            HttpResponseMessage response = await client.GetAsync("/api/Users/userinfo");
            if (response.IsSuccessStatusCode)
            {
                string data = await response.Content.ReadAsStringAsync();
                // Convert the JSON string into an Account object
                Account account = JsonConvert.DeserializeObject<Account>(data);
                HttpContext.Session.SetString("LoginID", account.Student.Name);
                HttpContext.Session.SetString("Role", "Student");
                // Store login datetime in session as a string
                HttpContext.Session.SetString("Datetime", @DateTime.Now.ToString());
                return RedirectToAction("Index", "Book");
            }
            return RedirectToAction("Index");
        }

        public ActionResult StaffMain()
        {
            // Stop accessing the action if not logged in or account not in the "staff" role
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Staff"))
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public ActionResult LogOut()
        {
            // Clear all key-values pairs stored in session state
            HttpContext.Session.Clear();
            // Call the Index action of Home Controller
            return RedirectToAction("Index");
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
